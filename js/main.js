var dsnv = [];
var dataJson = localStorage.getItem("DSNV_LOCAL");
if (dataJson != null) {
  var dataArr = JSON.parse(dataJson);
  for (var i = 0; i < dataArr.length; i++) {
    var item = dataArr[i];
    var nv = new NhanVien(
      item.taikhoan,
      item.ten,
      item.email,
      item.matkhau,
      item.ngaylam,
      item.luongCB,
      item.chucvu,
      item.giolam
    );
    dsnv.push(nv);
  }
  renderDSNV(dsnv);
}
function saveData(dsnv) {
  localStorage.setItem("DSNV_LOCAL", JSON.stringify(dsnv));
}
function themNhanVien() {
  var nv = layThongTinTuForm();
  var isValid =
    kiemTraRong("tbTKNV", nv.taikhoan) &&
    kiemTraTK(nv.taikhoan) &&
    kiemTraTKTrung(nv.taikhoan, dsnv);
  isValid = isValid & kiemTraRong("tbHoTen", nv.ten) && kiemTraTen(nv.ten);
  isValid =
    isValid & kiemTraRong("tbEmail", nv.email) &&
    kiemTraEmail(nv.email) &&
    kiemTraEmailTrung(nv.email, dsnv);
  isValid =
    isValid & kiemTraRong("tbMatKhau", nv.matkhau) && kiemTraMK(nv.matkhau);
  isValid = isValid & kiemTraRong("tbNgay", nv.ngaylam);
  isValid =
    isValid & kiemTraRong("tbLuongCB", nv.luongCB) && kiemTraLuong(nv.luongCB);
  isValid = isValid & kiemTraRong("tbChucVu", nv.chucvu);
  isValid =
    isValid & kiemTraRong("tbGiolam", nv.giolam) && kiemTraGioLam(nv.giolam);
  if (isValid) {
    dsnv.push(nv);
    var dataJson = JSON.stringify(dsnv);
    localStorage.setItem("DSNV_LOCAL", dataJson);
  }
  renderDSNV(dsnv);
  console.log(dsnv);
}
function capNhatNhanVien() {
  var nv = layThongTinTuForm();
  var isValid = kiemTraRong("tbHoTen", nv.ten) && kiemTraTen(nv.ten);
  isValid =
    isValid & kiemTraRong("tbEmail", nv.email) && kiemTraEmail(nv.email);
  isValid =
    isValid & kiemTraRong("tbMatKhau", nv.matkhau) && kiemTraMK(nv.matkhau);
  isValid = isValid & kiemTraRong("tbNgay", nv.ngaylam);
  isValid =
    isValid & kiemTraRong("tbLuongCB", nv.luongCB) && kiemTraLuong(nv.luongCB);
  isValid = isValid & kiemTraRong("tbChucVu", nv.chucvu);
  isValid =
    isValid & kiemTraRong("tbGiolam", nv.giolam) && kiemTraGioLam(nv.giolam);
  document.getElementById("tknv").disabled = false;
  var viTri = dsnv.findIndex(function (item) {
    return item.taikhoan == nv.taikhoan;
  });
  if (viTri !== -1) {
    if (isValid) {
      dsnv[viTri] = nv;
      renderDSNV(dsnv);
      resetForm();
      var dataJson = JSON.stringify(dsnv);
      localStorage.setItem("DSNV_LOCAL", dataJson);
    }
  }
}
function xoaNV(id) {
  var viTri = -1;
  for (var i = 0; i < dsnv.length; i++) {
    var nv = dsnv[i];
    if (nv.taikhoan == id) {
      viTri = i;
      break;
    }
  }
  dsnv.splice(viTri, 1);
  saveData(dsnv);
  renderDSNV(dsnv);
}
function suaNV(id) {
  var viTri = dsnv.findIndex((item) => {
    return item.taikhoan == id;
  });
  console.log(viTri);
  if (viTri != -1) {
    showThongTinLenForm(dsnv[viTri]);
    document.getElementById("tknv").disabled = true;
  }
}
function resetForm() {
  document.getElementById("formQLNV").reset();
}
function searchNV() {
  var timNV = document.getElementById("searchName").value;
  document.getElementById("searchName").value = "";
  var result = dsnv.filter((search) => {
    return search.xepLoai() == timNV;
  });
  renderDSNV(result);
}
