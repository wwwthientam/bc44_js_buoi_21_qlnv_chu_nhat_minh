function NhanVien(
  _taikhoan,
  _ten,
  _email,
  _matkhau,
  _ngaylam,
  _luongCB,
  _chucvu,
  _giolam
) {
  this.taikhoan = _taikhoan;
  this.ten = _ten;
  this.email = _email;
  this.matkhau = _matkhau;
  this.ngaylam = _ngaylam;
  this.luongCB = _luongCB;
  this.chucvu = _chucvu;
  this.giolam = _giolam;
  this.tongLuong = function () {
    var sum = 0;
    switch (this.chucvu) {
      case "Sếp":
        sum = this.luongCB * 3;
        break;
      case "Trưởng phòng":
        sum = this.luongCB * 2;
        break;
      case "Nhân viên":
        sum = this.luongCB * 1;
        break;
      default:
        break;
    }
    return sum;
  };
  this.xepLoai = function () {
    if (this.giolam >= 192) {
      return "Xuất Sắc";
    } else if (this.giolam >= 176) {
      return "Giỏi";
    } else if (this.giolam >= 160) {
      return "Khá";
    } else {
      return "Trung Bình";
    }
  };
}
