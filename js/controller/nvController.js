function renderDSNV(ArrNV) {
  var contentHTML = "";
  for (var i = 0; i < ArrNV.length; i++) {
    var a = ArrNV[i];
    var contentTr = `
<tr>
<td> ${a.taikhoan}</td>
<td> ${a.ten}</td>
<td> ${a.email}</td>
<td> ${a.ngaylam}</td>
<td> ${a.chucvu}</td>
<td> ${a.tongLuong()}</td>
<td> ${a.xepLoai()}</td>
<td>
<button onclick="suaNV(${
      a.taikhoan
    })" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Sửa</button>
    <button onclick="xoaNV(${a.taikhoan})" class="btn btn-danger">Xóa</button>
</td>
</tr>`;
    contentHTML += contentTr;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}
function layThongTinTuForm() {
  var taikhoan = document.getElementById("tknv").value;
  var ten = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var matkhau = document.getElementById("password").value;
  var ngaylam = Date(document.getElementById("datepicker").value);
  var luongCB = document.getElementById("luongCB").value * 1;
  var chucvu = document.getElementById("chucvu").value;
  var giolam = document.getElementById("gioLam").value * 1;
  var nv = new NhanVien(
    taikhoan,
    ten,
    email,
    matkhau,
    ngaylam,
    luongCB,
    chucvu,
    giolam
  );
  return nv;
}
function showThongTinLenForm(nv) {
  document.getElementById("tknv").value = nv.taikhoan;
  document.getElementById("name").value = nv.ten;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.matkhau;
  document.getElementById("datepicker").value = nv.ngaylam;
  document.getElementById("luongCB").value = nv.luongCB;
  document.getElementById("gioLam").value = nv.giolam;
  document.getElementById("chucvu").value = nv.chucvu;
}
