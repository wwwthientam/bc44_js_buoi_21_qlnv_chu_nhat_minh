var showMessage = function (id, message) {
  document.getElementById(id).innerHTML = message;
};
var kiemTraTKTrung = function (taikhoan, dsnv) {
  var index = dsnv.findIndex((item) => {
    return taikhoan == item.taikhoan;
  });
  if (index == -1) {
    showMessage("tbTKNV", "");
    return true;
  } else {
    showMessage("tbTKNV", `<p>Mã tài khoản này đã bị trùng</p>`);
    return false;
  }
};
var kiemTraEmailTrung = function (email, dsnv) {
  var index = dsnv.findIndex((item) => {
    return email == item.email;
  });
  if (index == -1) {
    showMessage("tbEmail", "");
    return true;
  } else {
    showMessage("tbEmail", `<p>Email này đã bị trùng</p>`);
    return false;
  }
};
var kiemTraTK = function (taikhoan) {
  if (/^[0-9]{4,6}$/.test(taikhoan)) {
    showMessage("tbTKNV", "");
    return true;
  } else {
    showMessage("tbTKNV", "Mã tài khoản nhân viên chỉ được nhập 4-6 ký số!");
    return false;
  }
};
var kiemTraTen = function (ten) {
  if (/^[A-Za-z ]+$/.test(ten)) {
    showMessage("tbHoTen", "");
    return true;
  } else {
    showMessage("tbHoTen", "Tên nhân viên không được chứa số hay ký tự");
    return false;
  }
};
var kiemTraEmail = function (email) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if (re.test(email)) {
    showMessage("tbEmail", "");
    return true;
  } else {
    showMessage("tbEmail", "Email này không hợp lệ");
    return false;
  }
};
var kiemTraMK = function (matkhau) {
  const mk = /^(?=.*\d)(?=.*[A-Z])(?=.*\W)[\dA-Za-z\W]{6,10}$/;
  if (mk.test(matkhau)) {
    showMessage("tbMatKhau", "");
    return true;
  } else {
    showMessage(
      "tbMatKhau",
      "Mật khẩu bắt buộc 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)"
    );
    return false;
  }
};
var kiemTraLuong = function (luongCB) {
  if (luongCB >= 1000000 && luongCB <= 20000000) {
    showMessage("tbLuongCB", "");
    return true;
  } else {
    showMessage("tbLuongCB", "Lương cơ bản nhân viên phải từ 1-20 triệu VNĐ!");
    return false;
  }
};
var kiemTraGioLam = function (giolam) {
  if (giolam >= 80 && giolam <= 200) {
    showMessage("tbGiolam", "");
    return true;
  } else {
    showMessage("tbGiolam", "Số giờ làm trong tháng phải từ 80 - 200 giờ!");
    return false;
  }
};
var kiemTraRong = function (taikhoanErr, value) {
  if (value.length == 0 || !value) {
    showMessage(taikhoanErr, "Mục này không được bỏ trống!");
    return false;
  } else {
    showMessage(taikhoanErr, "");
    return true;
  }
};
